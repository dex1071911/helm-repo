[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/lg08-helm-charts)](https://artifacthub.io/packages/search?repo=lg08-helm-charts)


This is the helm repo we are using for all of Dex's custom helm charts. Artifact Hub above is a great spot to browse all the charts and has easy installation instructions so I recommend to just look there :)
Just click the link above ^^



```bash
helm repo add lg08-helm-charts https://gitlab.com/api/v4/projects/55761064/packages/helm/helm-repo
```


NOTE:
This is just the repository where we create the helm charts for the apps that we support.
For configuration files and details on how we actually deploy them, see the dex_k8s repository linked here: TODO (input link here)