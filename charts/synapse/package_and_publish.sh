#!/bin/bash

# run this file to package and publish the helm chart to the gitlab helm repository
# run this file like so:
# infisical run --env=dev -- ./package_and_publish.sh
VERSION=$(cat Chart.yaml | grep -m 1 version: | awk '{print $2}')
echo "Packaging version $VERSION"

PACKAGE_NAME="matrix-$VERSION.tgz"

helm package ./
# curl --request POST\
#      --form 'chart=@synapse-0.1.0.tgz' \
#      --user lg08:$GITLAB_ACCESS_TOKEN \
#      https://gitlab.com/api/v4/projects/55761064/packages/helm/api/helm-repo/charts
curl --user lg08:$CODEBERG_PASSWORD -X POST --upload-file ./$PACKAGE_NAME https://codeberg.org/api/packages/dex/helm/api/charts

rm ./$PACKAGE_NAME