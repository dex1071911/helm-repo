# Description
This chart is used to deploy d3x's whole matrix functionality for users.

Currently this means that we are installing:
- a synapse homeserver, setup with federation, services, ingresses and everything else that's needed to just getup and go
- an admin dashboard that allows admins to easiliy manage the people who are allowed to make accounts and use their matrix server
- an InfisicalSecret CRD which allows us to manage all the secrets used in this app from one external location


# Installation
to install this helm chart, just modify all the values in the values.yaml file according to your needs and then add our repo and then deploy it.